//          Copyright Rohit Grover 2018 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef _XFORMS_HPP
#define _XFORMS_HPP

#include "map.hpp"
#include "range.hpp"
#include "scalars.hpp"

#include <boost/yap/algorithm.hpp>

struct toRange
{
    template <typename Callable, typename RangeExpr>
    constexpr auto operator()(boost::yap::expr_tag<boost::yap::expr_kind::shift_left_assign>,
                              map_function<Callable> m,
                              RangeExpr rightSubexpr)
    {
        auto rightSubrange = boost::yap::transform(rightSubexpr, toRange{});

        using underlying_iterator     = typename decltype(rightSubrange)::iterator;
        using underlying_iterator_ref = std::add_lvalue_reference_t<underlying_iterator>;
        using underlying_value_type   = typename underlying_iterator::value_type;
        static_assert(std::is_invocable_v<Callable, underlying_value_type>,
                      "mapping function incompatible with source range");

        auto callable = m.callable;

        auto incr = [end = rightSubrange.end](underlying_iterator_ref iter) -> void {
            if (iter != end) {
                ++iter;
            }
        };
        auto deref = [xform = callable](underlying_iterator iter) {
            return xform(*iter);
        };

        return range{range_iterator{rightSubrange.begin, incr, deref},
                     range_iterator{rightSubrange.end, incr, deref}};
    }

    template <typename Callable, typename RangeIterator>
    constexpr auto operator()(boost::yap::expr_tag<boost::yap::expr_kind::shift_left_assign>,
                              map_function<Callable>& m,
                              range<RangeIterator> r)
    {
        using underlying_iterator     = RangeIterator;
        using underlying_iterator_ref = std::add_lvalue_reference_t<underlying_iterator>;
        using underlying_value_type   = typename underlying_iterator::value_type;
        static_assert(std::is_invocable_v<Callable, underlying_value_type>,
                      "mapping function incompatible with source range");

        auto callable = m.callable;

        auto incr = [end = r.end](underlying_iterator_ref iter) -> void {
            if (iter != end) {
                ++iter;
            }
        };
        auto deref = [xform = callable](underlying_iterator iter) {
            return xform(*iter);
        };

        return range{range_iterator{r.begin, incr, deref},
                     range_iterator{r.end, incr, deref}};
    }

    template <typename Predicate, typename RangeExpr>
    constexpr auto operator()(boost::yap::expr_tag<boost::yap::expr_kind::shift_left_assign>,
                              filter_function<Predicate> f,
                              RangeExpr rightSubexpr)
    {
        auto rightSubrange = boost::yap::transform(rightSubexpr, toRange{});

        using underlying_iterator     = typename decltype(rightSubrange)::iterator;
        using underlying_iterator_ref = std::add_lvalue_reference_t<underlying_iterator>;
        using underlying_value_type   = typename underlying_iterator::value_type;
        static_assert(std::is_invocable_v<Predicate, underlying_value_type>,
            "filter function incompatible with source range");

        auto predicate = f.predicate;

        auto incr = [predicate, end = rightSubrange.end](underlying_iterator_ref iter) -> void {
            ++iter;
            while ((iter != end) && !predicate(*iter)) {
                ++iter;
            }
        };
        auto deref = [](underlying_iterator iter) {
            return *iter;
        };

        if ((rightSubrange.begin != rightSubrange.end) && !predicate(*rightSubrange.begin)) {
            incr(rightSubrange.begin);
        }

        return range{range_iterator{rightSubrange.begin, incr, deref},
                     range_iterator{rightSubrange.end, incr, deref}};
    }

    template <typename Predicate, typename RangeIterator>
    constexpr auto operator()(boost::yap::expr_tag<boost::yap::expr_kind::shift_left_assign>,
                              filter_function<Predicate>& f,
                              range<RangeIterator> r)
    {
        using underlying_iterator     = RangeIterator;
        using underlying_iterator_ref = std::add_lvalue_reference_t<underlying_iterator>;
        using underlying_value_type   = typename underlying_iterator::value_type;
        static_assert(std::is_invocable_v<Predicate, underlying_value_type>,
                      "filter function incompatible with source range");

        auto predicate = f.predicate;

        auto incr = [predicate, end = r.end](underlying_iterator_ref iter) -> void {
            ++iter;
            while ((iter != end) && !predicate(*iter)) {
                ++iter;
            }
        };
        auto deref = [](underlying_iterator iter) {
            return *iter;
        };

        if ((r.begin != r.end) && !predicate(*r.begin)) {
            incr(r.begin);
        }

        return range{range_iterator{r.begin, incr, deref},
                     range_iterator{r.end, incr, deref}};
    }

    template <typename RangeExpr>
    constexpr auto operator()(boost::yap::expr_tag<boost::yap::expr_kind::shift_left_assign>,
                              take_value take,
                              RangeExpr rightSubexpr)
    {
        auto rightSubrange = boost::yap::transform(rightSubexpr, toRange{});

        using underlying_iterator = typename decltype(rightSubrange)::iterator;
        using underlying_iterator_ref = std::add_lvalue_reference_t<underlying_iterator>;

        auto incr = [remaining = take.n - 1, end = rightSubrange.end](underlying_iterator_ref iter) mutable {
            if ((iter != end) && (remaining > 0)) {
                ++iter;
                --remaining;
            } else {
                iter.terminateIteration();
            }
        };
        auto deref = [](underlying_iterator iter) { return *iter; };

        return range{range_iterator{rightSubrange.begin, incr, deref},
                     range_iterator{rightSubrange.end, incr, deref}};
    }

    template <typename RangeIterator>
    constexpr auto operator()(boost::yap::expr_tag<boost::yap::expr_kind::shift_left_assign>,
                              take_value take,
                              range<RangeIterator> r)
    {
        using underlying_iterator = RangeIterator;
        using underlying_iterator_ref = std::add_lvalue_reference_t<underlying_iterator>;

        auto incr = [remaining = take.n - 1, end = r.end](underlying_iterator_ref iter) mutable {
            if ((iter != end) && (remaining > 0)) {
                ++iter;
                --remaining;
            } else {
                iter.terminateIteration();
            }
        };
        auto deref = [](underlying_iterator iter) { return *iter; };

        return range{range_iterator{r.begin, incr, deref},
                     range_iterator{r.end, incr, deref}};
    }
};

#endif //_XFORMS_HPP
