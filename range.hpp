//          Copyright Rohit Grover 2018 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef TRYING_YAP_RANGE_HPP
#define TRYING_YAP_RANGE_HPP

#include <boost/yap/algorithm.hpp>
#include <boost/callable_traits/return_type.hpp>

template <typename Iter>
struct range
{
    using iterator = Iter;

    Iter begin;
    Iter end;

    template <typename Assignee>
    constexpr operator Assignee() const
    {
        return {begin, end};
    }
};
template <typename T> range(T, T) -> range<T>;

template <boost::yap::expr_kind Kind, typename Tuple>
struct range_expr
{
    constexpr static boost::yap::expr_kind kind = Kind;
    Tuple                                  elements;
};

template <typename T>
struct expose;

template <typename UnderlyingIterator, typename IncrementFunction, typename DereferenceFunction>
struct range_iterator
{
    using iterator_category = std::input_iterator_tag;
    using value_type        = std::invoke_result_t<DereferenceFunction, UnderlyingIterator>;
    using difference_type   = size_t;
    using pointer           = value_type*;
    using reference         = value_type&;

    UnderlyingIterator  underlyingIter;
    IncrementFunction   increment;
    DereferenceFunction deref;

    constexpr range_iterator(UnderlyingIterator underlyingIterator, IncrementFunction increment,
                             DereferenceFunction deref)
        : underlyingIter{underlyingIterator}, increment{increment}, deref{deref}
    {
    }

    constexpr void operator++() { increment(underlyingIter); }

    constexpr value_type operator*() { return deref(underlyingIter); }

    constexpr bool operator!=(range_iterator<UnderlyingIterator, IncrementFunction, DereferenceFunction> end) const
    {
        return underlyingIter != end.underlyingIter;
    }

    constexpr void terminateIteration() {
        underlyingIter.terminateIteration();
    }
};


#endif //TRYING_YAP_RANGE_HPP
