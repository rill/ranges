//          Copyright Rohit Grover 2018 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef TRYING_YAP_SCALARS_HPP
#define TRYING_YAP_SCALARS_HPP

#include "scalar_iterator.hpp"
#include <boost/yap/algorithm.hpp>

using namespace boost::hana::literals;

template <typename T>
struct scalars
{
    using value_type = T;
    using iterator_type = scalar_iterator<T>;

    constexpr static boost::yap::expr_kind kind = boost::yap::expr_kind::terminal;

    using tuple_element_type = range<iterator_type>;
    boost::hana::tuple<tuple_element_type> elements;

    constexpr scalars() : elements{{{T{}, T{1}}, {T{}, T{1}, true}}} {}
    constexpr scalars(T start) : elements{{{start, T{1}}, {start, T{1}, true}}} {}
    constexpr scalars(T start, T end) : elements{{{start, T{1}}, {end, T{}}}} {}
    constexpr scalars(T start, T step, T end) : elements{{{start, step}, {end, T{}}}} {}

    constexpr typename iterator_type::difference_type size() const {
        return elements[0_c].end - elements[0_c].begin;
    }

    constexpr range<iterator_type> asRange()
    {
        return elements[0_c];
    }

    template <typename Assignee>
    constexpr operator Assignee() const
    {
        return {asRange().begin, asRange().end};
    }
};

#endif //TRYING_YAP_SCALARS_HPP
