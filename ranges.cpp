//          Copyright Rohit Grover 2018 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#include "range.hpp"
#include "scalars.hpp"
#include "map.hpp"
#include "filter.hpp"
#include "take.hpp"
#include "xforms.hpp"

#include <cmath>
#include <vector>
#include <iostream>

template <typename T>
struct expose;

using namespace boost::yap;

int squared(int x) { return x * x; }
int subtract1(int x) {return x - 1;}
bool odd(int x) {return (x % 2) == 1;}

double sq(double x) {
    return std::sqrt(x);
}

int main()
{
    auto e = take(3)
             <<= filter([](int x){
                     return (x % 5) == 1;
                 })
             <<= map(squared)
             <<= filter(odd)
             <<= scalars<int>{2};
    std::vector<double> v = boost::yap::transform(e, toRange{});

    for (auto value : v) {
        std::cout << value << std::endl;
    }
}

