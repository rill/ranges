//          Copyright Rohit Grover 2018 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef _IS_CALLABLE_HPP
#define _IS_CALLABLE_HPP

#include <type_traits>

template< typename, typename = std::void_t<>>
struct is_callable_impl : std::false_type { };

template <typename T>
struct is_callable_impl<T, std::void_t<decltype( &T::operator() )>> : std::true_type { };

template <typename T>
struct is_callable_impl<T, std::enable_if_t<std::is_function<T>::value>> : std::true_type { };

template <typename T>
using is_callable_t = is_callable_impl<std::remove_pointer_t<std::remove_reference_t<T>>>;

template <typename T>
constexpr auto is_callable_v = is_callable_t<T> {};

template <typename F>
constexpr bool is_callable(F const&) {
    return is_callable_v<F>;
}

#endif //_IS_CALLABLE_HPP
